<?php
require_once 'db.php';
$mysqli = new mysqli($hm, $un, $pw, $db);
if ($mysqli->connect_error) die($mysqli->connect_error);
$mysqli->set_charset("utf8");

$user_login = $_POST["user_login"];

$stmt=$mysqli->stmt_init();
if(!$stmt->prepare("SELECT user_login FROM users WHERE user_login = ?"))
{
    exit("Ошибка подготовки запроса\n");
}
$stmt->bind_param("s", $user_login);
if (!$stmt->execute())
{
    exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
}
if (!$stmt->bind_result($login))
{
    exit("Не удалось привязать выходные параметры: (" . $stmt->errno . ") " . $stmt->error);
}
if ($row=$stmt->fetch())
{
    echo("Этот email уже занят");
}
else
{
    if($_POST['user_login']!='')
    {
        echo("e-mail свободен");
    }
}
$stmt->close();
$mysqli->close();
?>

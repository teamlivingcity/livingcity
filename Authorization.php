<?php
require_once 'db.php';
$mysqli = new mysqli($hm, $un, $pw, $db);
if ($mysqli->connect_error) die($mysqli->connect_error);
$mysqli->set_charset("utf8");

if ($_POST["button"]) {
    $user_login = $_POST["user_login"];
    //////
    $stmt = $mysqli->stmt_init();
    if (!$stmt->prepare("SELECT id_user, user_login, user_password FROM users WHERE user_login = ?")) {
        exit("Ошибка подготовки запроса\n");
    }

    /////
    $stmt->bind_param("s", $user_login);
    if (!$stmt->execute()) {
        exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
    }

    if (!$stmt->bind_result($id_user, $user_login, $user_password)) {
        exit("Не удалось привязать выходные параметры: (" . $stmt->errno . ") " . $stmt->error);
    }

    if (!($row = $stmt->fetch())) {
        echo "Неверный логин";
    } else {
        if (password_verify($_POST['user_password'], $user_password)) {
            session_start();

            if (isset($_POST['remember_me'])) {
                //setcookie('user_login', password_hash($_POST['user_login'], PASSWORD_DEFAULT), time()+86400);
                //setcookie('user_password', $_POST['user_password'], time() + 86400);
                setcookie('user_password', password_hash($_POST['user_password'], PASSWORD_DEFAULT), time()+86400);
                setcookie('user_login', $_POST['user_login'], time() + 86400);
                $_SESSION['user_login'] = $_POST['user_login'];
                $_SESSION['user_password'] = $_POST['user_password'];
            } else {
                //setcookie('user_login', password_hash($_POST['user_login'], PASSWORD_DEFAULT), 0);
                //setcookie('user_password', password_hash($_POST['user_password'], PASSWORD_DEFAULT),0);
                //setcookie('user_login', $_POST['user_login'],  0);
                //setcookie('user_password', $_POST['user_password'],  0);
                //setcookie('idses', session_id(), time() + 86400);
                $_SESSION['user_login'] = $_POST['user_login'];
                $_SESSION['user_password'] = $_POST['user_password'];
            }
            exit(header("Location:lc?id=$id_user"));
        } else {
            echo "Неверный пароль";
        }
    }
    $stmt->close();
}
$mysqli->close();
?>
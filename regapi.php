<?php
session_start();
//подключение файла с информацией о бд
require_once 'db.php';
//подключение файла с помощью которого будет отправляться письмо проверки подлинности email
require_once 'send_mail.php';
//создание подключения с бд
$mysqli = new mysqli($hm, $un, $pw, $db);
//проверка подключения на ошибки
if ($mysqli->connect_error) die($mysqli->connect_error);
//выбор кодировки utf-8, чтобы русские символы передавались в базу корректно
$mysqli->set_charset("utf8");
//проверка значения нажатия кнопки 'button', переданое методом post
if ($_POST["button"]) {
    //инициализация переменных, с помощью данных переданных из формы методом POST
    $user_login = $_POST["user_login"];
    $user_password = password_hash($_POST["user_password"], PASSWORD_DEFAULT);
    $user_name = $_POST["user_name"];
    $user_surname = $_POST["user_surname"];
    $user_phone = $_POST["user_phone"];
    $user_address = $_POST["user_address"];
    $user_birthday = date($_POST["user_birthday"]);
    //создание кода активации пользователя
    $activation = password_hash($_POST["user_login"], PASSWORD_DEFAULT);
    //создание подготовленного запроса
    $stmt = $mysqli->stmt_init();
    //Проверка доступности email
    //создание строки запроса
    if (!$stmt->prepare("SELECT user_login FROM users WHERE user_login = ?")) {
        exit("Ошибка подготовки запроса\n");
    }
    //внесение входных параметров в запрос
    $stmt->bind_param("s", $user_login);
    //выполнение запроса
    if (!$stmt->execute()) {
        exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
    }
    //внесение выходных параметров
    if (!$stmt->bind_result($login)) {
        exit("Не удалось привязать выходные параметры: (" . $stmt->errno . ") " . $stmt->error);
    }
    //если запрос вернулся не пустой, то логин занят
    if ($row = $stmt->fetch()) {
        exit("Этот email уже занят");
    }
    //закрытие запроса
    $stmt->close();
//////////////////////////////////////////////////////////////////////////////////
/// создание запроса, для добавления пользователя в бд
    $stmt = $mysqli->stmt_init();
    if (!$stmt->prepare("INSERT INTO users (user_login,user_password,user_name,user_surname,user_phone,user_address,user_birthday,user_image,user_wallet,fk_group,fk_organization) 
        VALUES (?,?,?,?,?,?,?, NULL, NULL, DEFAULT, NULL)")) {
        exit("Ошибка подготовки запроса\n");
    }
    $stmt->bind_param("sssssss", $user_login, $user_password, $user_name, $user_surname, $user_phone, $user_address, $user_birthday);
    if (!$stmt->execute()) {
        exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
    } else {
        //запрос id только что зарегестрированного пользователя
        $stmt = $mysqli->stmt_init();
        if (!$stmt->prepare("SELECT id_user FROM users WHERE user_login = ?")) {
            exit("Ошибка подготовки запроса\n");
        }

        $stmt->bind_param("s", $user_login);
        if (!$stmt->execute()) {
            exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
        }

        if (!$stmt->bind_result($id_user)) {
            exit("Не удалось привязать выходные параметры: (" . $stmt->errno . ") " . $stmt->error);
        }

        if (!($row = $stmt->fetch())) {
            echo "Неверный логин";
        } else {
            //если запрос выполнен успешно, то сохраняем данные пользователя в куках и сессии для дальнейшего использования/////////////////////////////////////////////////////////////////////////////////////////
            //в сессиях и куках хранится id пользователя, в куках храниться хэш пароля, в сессиях храниться пароль.////
            //мы предполагаем, что сессии безопасны, поэтому пока так. Возможно куки будут заменены на токен,
            //в сессиях будет храниться что-то другое
            setcookie('user_password', password_hash($_POST['user_password'], PASSWORD_DEFAULT), time()+86400);
            setcookie('id_user', $id_user, time() + 86400);
            $_SESSION['id_user'] = $id_user;
            $_SESSION['user_password'] = $_POST['user_password'];
            //подготовка к отправке письма с подтверждением корректности email
            //в переменную $to записывается email пользователя
            $to = $user_login;
            //тема письма
            $subject = "Подтверждение электронной почты";
            //содержание письма
            $body = 'Здравствуйте! <br/> <br/> Мы должны убедиться в том, что вы человек. Пожалуйста, подтвердите адрес вашей электронной почты, и можете начать использовать ваш аккаунт на сайте. <br/> <br/> <a href="' . $base_url . 'activation/' . $activation . '">' . $base_url . 'activation/' . $activation . '</a>';

            $to = $user_login;
            $subject = "Подтверждение электронной почты";
            $body = 'Здравствуйте! <br/> <br/> Мы должны убедиться в том, что вы человек. Пожалуйста, подтвердите адрес вашей электронной почты, и можете начать использовать ваш аккаунт на сайте. <br/> <br/> <a href="' . $base_url . 'activation/' . $activation . '">' . $base_url . 'activation/' . $activation . '</a>';
            send_mail($to, $subject, $body);
            die("Вам отправлено письмо");
        }
        $stmt->close();
    }
}
$mysqli->close();
?>
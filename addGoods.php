<?php
/**
 * Created by PhpStorm.
 * User: Kolos
 * Date: 06.07.2017
 * Time: 18:01
 */

session_start();
require_once 'db.php';
require_once 'checkIt.php';
$mysqli = new mysqli("$hm", "$un", "$pw", "$db");
$mysqli->set_charset("utf8");

$user_inf = sesOrCock($mysqli);//получение ассоциативного массива с логином и паролем из кук, либо сессии
$user_login = $user_inf['user_login'];

$goods_name = $_POST['goods_name'];
$goods_address = $_POST['goods_address'];
$goods_description = $_POST['goods_description'];
/////получение fk_category через значение value в select//////
if (!($fk_category = ++$_POST['goods_category'])) {
    die("Ошибка чтения категории");
}
///////////////////////////////////////////////
/// получение fk_price через начение value в select////
if (!($fk_price = ++$_POST['goods_price'])) {
    die("Ошибка чтения категории");
}
///////////////////////////////////////////////////////////
////////получение fk_user//////////////////////////////////
//$query = "SELECT id_user FROM `users` WHERE user_login=$user_login";
$stmt = $mysqli->stmt_init();
//Получение id_user
if (!$stmt->prepare("SELECT id_user FROM users WHERE user_login = ?")) {
    exit("Ошибка подготовки запроса\n");
}
$stmt->bind_param("s", $user_login);
if (!$stmt->execute()) {
    exit("Не удалось выполнить запрос1: (" . $mysqli->errno . ") " . $mysqli->error);
}
if (!$stmt->bind_result($fk_user)) {
    exit("Не удалось привязать выходные параметры: (" . $stmt->errno . ") " . $stmt->error);
}
if (!($row = $stmt->fetch())) {
    exit("Не удалось добавить товар");
}
/////////////////////////////////////////
//////////////////////////////////////////
/// создание и выполнение подготовленного запроса для добавления товара в таблицу goods////
//die("Имя товара".$goods_name."; Адрес товара" . $goods_address."; Описание товара " . $goods_description."; фк юсер" . $fk_user. "; Цена товара " . $fk_price. "; Фк категории" . $fk_category);
$stmt->prepare("INSERT INTO goods (good_name, goods_address, goods_img, goods_description, fk_user, fk_price, fk_category)
                      VALUES(?,?,NULL,?,?,?,?)");
$stmt->bind_param("ssssss", $goods_name, $goods_address, $goods_description, $fk_user, $fk_price,$fk_category);
$stmt->execute();
if (!$stmt->execute()) {
    exit("Не удалось выполнить запрос: (" . $mysqli->errno . ") " . $mysqli->error);
} else {
    echo "товар добавлен";
}
$stmt->close();
?>